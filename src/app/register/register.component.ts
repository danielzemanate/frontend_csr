import { Component,OnInit } from "@angular/core";
import { AuthenticationService, TokenPayload } from "../authentication.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: "./register.component.html"
})
export class RegisterComponent implements OnInit {
  datos: FormGroup;
  submitted = false;
  // GENERAR USUARIO CON UNAS CREDENCIALES Y TOKEN
  credentials: TokenPayload = {
    id: 0,
    nombre: "",
    apellido: "",
    correo: "",
    password: ""
  };

// INICIALIZAMOS LOS SERVICIOS QUE NECESITAMOS
  constructor(private auth: AuthenticationService, private router: Router,
    private formBuilder: FormBuilder) {}

    ngOnInit() {
      this.datos = this.formBuilder.group({
        nombres: ['', Validators.required], 
        apellidos: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]], 
        password: ['', [Validators.required, Validators.minLength(6)]],
      });
    }

    // FUNCION QUE COMPRUEBA SI EL CAMPO INPUT ESTA VACIO
    get f() {
      return this.datos.controls;
    }

    // SUBMIT DELA FUNCION REGISTRAR
    onSubmit() {
      this.submitted = true;
  
      if (this.datos.invalid) {
        return;
      }
      this.register();
    }
  
    // FUNCION REGISTRAR CON SERVICIO
  register() {
    this.auth.register(this.credentials).subscribe(
      () => {
        //console.log(this.credentials);
          this.router.navigateByUrl("/");
        
        
      },
      err => {
        console.error(err);
      }
    );
  }
}