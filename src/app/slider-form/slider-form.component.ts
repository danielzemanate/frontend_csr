import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { ImagesService } from "../services/images.service";

@Component({
  selector: 'app-slider-form',
  templateUrl: './slider-form.component.html',
  styleUrls: ['./slider-form.component.css']
})
export class SliderFormComponent implements OnInit {
  imagenForm: FormGroup;
  // ARCHIVOS SELECCIONADOS
  public fileSelected: File = null;

  // GUARDAR ARCHIVOS EN UN ARRAY
  public files: any = [];

  // NOMBRE IMAGEN PARA MOSTRAR EN PANTALLA
  public imagen_name=[];

  constructor( 
    private imageService: ImagesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    //INICIALIZAR VALIDACIONES
    this.formImage()

    //TRAER IMAGENES 
    this.getArchivos()
  }

  // FUNCION PARA VALIDAR CAMPOS
  formImage () {
    this.imagenForm = this.formBuilder.group({
      archivo: [null, Validators.required],
    })
  }

  //CAMPOS VACIOS
  get nule() {
    return this.imagenForm.controls;
  }

  //SUBMIT SUBIR IMAGENES
  onSubmitUpload() {
    if (this.imagenForm.valid){
      this.onUploadFile()
    }
  }

  //SUBIR ARCHIVO
  async onUploadFile() {
    await this.imageService.uploadFileAsync(this.fileSelected,);
    this.showToasterUpload();
    this.getArchivos();
  }

  //EVENT ARCHIVO
  onFileChange(event) {
    this.fileSelected = event.target.files[0];
  }

  //TRAER FILES 
  getArchivos() {
      this.imageService.getArchivos().subscribe(
        (res) => {
          this.files = res;
          if(this.files.length===0){
          }
          for(let item of this.files){
          this.imagen_name = item.imagen_name;
          }
          // console.log(this.imagen_name)
          // console.log(this.files)
        },
        (err) => console.error(err)
      );
    
  }

  // TOASTER ARCHIVO CARGADO CORRECTAMENTE
  showToasterUpload() {
    this.toastr.success("File uploaded successfully");
  }

}
