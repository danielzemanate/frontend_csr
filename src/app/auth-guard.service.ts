import { Injectable } from '@angular/core'
import { Router, CanActivate } from '@angular/router'
import { AuthenticationService } from './authentication.service'

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private auth: AuthenticationService, private router: Router) {}
  
  // VERIFICAR SI ESTA LOGEADO PARA QUE NO PUEDA ACCEDER A OTROS LINKS MEDIANTE LA URL
  canActivate() {
    if (this.auth.isLoggedIn()) {
      this.router.navigateByUrl('/')
      return false
    }
    return true
  }
}