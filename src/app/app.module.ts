import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ToastrModule } from "ngx-toastr";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

// ROUTES 
import { AppFooterComponent } from "./app-footer/app-footer.component";
import { HomeComponent } from "./home/home.component";
import { RegisterComponent } from './register/register.component'
import { LoginComponent } from "./login/login.component";
import { AuthGuardService } from "./auth-guard.service";
import { AuthenticationService } from "./authentication.service";
import { ImagesService } from "./services/images.service";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SliderComponent } from './slider/slider.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SliderFormComponent } from './slider-form/slider-form.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardService],},
  { path: 'register', component: RegisterComponent },
  { path: 'slider', component: SliderComponent },
  { path: 'addImage', component: SliderFormComponent },
]
@NgModule({
  declarations: [
    AppComponent,
    AppFooterComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    SliderComponent,
    SliderFormComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    RouterModule.forRoot(routes),
    ToastrModule.forRoot(),
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule
  ],
  providers: [AuthenticationService,
              AuthGuardService,
              ImagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
