import { Component } from '@angular/core'
import { AuthenticationService, TokenPayload } from '../authentication.service'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {
  // CREDECIALES PARA INICIAR SESIÓN
  credentials: TokenPayload = {
    id: 0,
    nombre: '',
    apellido: '',
    correo: '',
    password: ''
  }
  //  INICIALIZAR SEVICIOS COMUNICACION BD
  constructor(private auth: AuthenticationService, private router: Router,
    private toastr: ToastrService) {}

  // FUNCION INICIAR SESION
  login() {
    this.auth.login(this.credentials).subscribe(
      () => {
        this.showToasterSuccess();
        this.router.navigateByUrl('/')
      },
      err => {
      this.showToasterError();
      }
    )
  }
  showToasterSuccess(){
    this.toastr.success("Authentication completed")
}

showToasterError(){
  this.toastr.error("Email or password incorrect.")
}
}