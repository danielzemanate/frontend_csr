import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { AppConfig } from "./app.config";
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  static getSettings(): Observable<AppConfig> {
    let settings = new AppConfig();
    return of<AppConfig>(settings);
  }
}
