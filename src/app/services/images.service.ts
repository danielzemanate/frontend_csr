import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Product } from "../models/product";
// import { Equipment } from "../models/equipments";
// import { Category } from "../models/category";
// import { Novedad } from "../models/novedades";
import { Observable } from "rxjs";
import { AppConfig } from "../config/app.config";
import { ConfigService } from "../config/config.service";

@Injectable({
  providedIn: "root",
})
export class ImagesService {
  private settings: AppConfig;
  public url: string = "";

  constructor(private http: HttpClient) {
    ConfigService.getSettings().subscribe(
      (settings) => (this.settings = settings),
      () => null,
      () => {
        this.url = `${this.settings.url}`;
      }
    );
  }


  //cargar fichero de productos al servidor y a la base de datos
  async uploadFileAsync(file: File) {
    let formData: FormData = new FormData();
    formData.append("uploadFile", file, file.name);
    return await this.http
      .post(this.url + "/uploadFile", formData)
      .toPromise()
      .then((res) => res)
      .catch((err) => "Error al subir el archivo");
  }

  //TRAER LOS ARCHIVOS
  getArchivos() {
    return this.http.get(`${this.url}/getListFiles`);
  }

}
