import { Component, OnInit } from '@angular/core';
import { ImagesService } from "../services/images.service";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

    // GUARDAR ARCHIVOS EN UN ARRAY
    public files: any = [];

     // NOMBRE IMAGEN PARA MOSTRAR EN PANTALLA
  public imagen_name=[];

  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

  constructor(private imageService: ImagesService,
    private http: HttpClient,
    ) { }

  ngOnInit(): void {
    this.images
    // TRAER IMAGENES BD
    this.getArchivos()
    // console.log(this.images)
  }

  //TRAER FILES 
  getArchivos() {
    this.imageService.getArchivos().subscribe(
      (res) => {
        this.files = res;
        if(this.files.length===0){
        }
        for(let item of this.files){
        this.imagen_name = item.imagen_name;
        }
        // console.log(this.imagen_name)
        console.log(this.files)
      },
      (err) => console.error(err)
    );
  
}

}
