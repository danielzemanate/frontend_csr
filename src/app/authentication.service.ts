import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router } from '@angular/router'
import { Token } from '@angular/compiler/src/ml_parser/lexer'
import { AppConfig } from "./config/app.config";
import { ConfigService } from "./config/config.service";

export interface UserDetails {
  id: number
  nombre: string
  apellido: string
  correo: string
  password: string
  exp: number
  iat: number
}

interface TokenResponse {
  token: string
}

export interface TokenPayload {
  id: number
  nombre: string
  apellido: string
  correo: string
  password: string
}

@Injectable()
export class AuthenticationService {
  private token: string;
  private settings: AppConfig;
  public url: string = "";

  constructor(private http: HttpClient, private router: Router) {
    ConfigService.getSettings().subscribe(
      settings => (this.settings = settings),
      () => null,
      () => {
        this.url = `${this.settings.url}`;
      }
    );
  }
// GUARDAR TOKEN PARA USAR DURANTE LA SESION
  private saveToken(token: string): void {
    localStorage.setItem('usertoken', token)
    this.token = token
  }

  // OBTENER TOKEN CUANDO SE NECECITE
  private getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('usertoken')
    }
    return this.token
  }

  // DETALLES DEL USUARIO LOGUEADO
  public getUserDetails(): UserDetails {
    const token = this.getToken()
    let payload
    if (token) {
      payload = token.split('.')[1]
      payload = window.atob(payload)
      return JSON.parse(payload)
    } else {
      return null
    }
  }
  // TIEMPO DE INICIO DE SESION INACTIVO
  public isLoggedIn(): boolean {
    const user = this.getUserDetails()
    if (user) {
      return user.exp > Date.now() / 2000
    } else {
      return false
    }
  }
    // FUNCION REGISTRAR UN NUEVO USUARIO, TOKEN AND OBSERVABLE
    public register(user: TokenPayload): Observable<any> {
      if(user){
        return this.http.post(`${this.url}/registrar`, user)
      }else {
        return null
      }
    }

  // FUNCION LOGIN PARA INGRESAR A LA APP (ENVIO TOKEN Y RETORNO USUARIO)
  public login(user: TokenPayload): Observable<any> {
    const base = this.http.post(`${this.url}/login`, user)
    const request = base.pipe(
      map((data: TokenResponse) => {
        if (data.token) {
          this.saveToken(data.token)
        }
        return data
      })
    )

    return request
  }
  // TRAER DATOS USUARIO DE LA BD
  public profile(): Observable<any> {
    return this.http.get(`/users/profile`, {
      headers: { Authorization: ` ${this.getToken()}` }
    })
  }
  // FUNCION SALIR Y REMOVERTOKEN
  public logout(): void {
    this.token = ''
    window.localStorage.removeItem('usertoken')
    this.router.navigateByUrl('/')
  }
}